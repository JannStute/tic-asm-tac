.PHONY: clean

linux:
	nasm -f elf64 -o tic_tac.o tic_tac.asm
	ld -o tic_tac tic_tac.o

clean:
	-rm tic_tac tic_tac.o