# Tic-ASM-Tac
A Tic-Tac-Toe Game written in x86_64 Assembly for linux.

# Install
## Prerequisites
- nasm
- make
- gcc

## Build
`make linux`

## Run
`./tic_tac`