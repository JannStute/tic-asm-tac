; https://cs61.seas.harvard.edu/site/2018/Asm2/
; https://www.cs.uaf.edu/2017/fall/cs301/reference/x86_64.html
; https://hackeradam.com/x86-64-linux-syscalls/

global _start

section .text

_start:
    ; Intro Text
    call printIntro

    ; init cells (0x00 = empty) ; is this even necessary?
    mov byte [cells+0], 0x00
    mov byte [cells+1], 0x00
    mov byte [cells+2], 0x00
    mov byte [cells+3], 0x00
    mov byte [cells+4], 0x00
    mov byte [cells+5], 0x00
    mov byte [cells+6], 0x00
    mov byte [cells+7], 0x00
    mov byte [cells+8], 0x00

    ; init Turn Counter
    XOR R9, R9

    ; init player (0x0F = Player 1, 0xF0 = Player 2)
    XOR R8, R8
    mov R8B, 0x0F

    gameLoop:
        cmp R8B, 0x0F
        je P1
        mov RDI, message_player_two
        mov RSI, message_player_two_l
        jmp end_P1
        P1:
        mov RDI, message_player_one
        mov RSI, message_player_one_l
        end_P1:
        call printl

        getCell:
            mov RDI, message_enter_cell
            mov RSI, message_enter_cell_l
            call getDigit

            ; ask again if output was 0, because that's not a cell index
            cmp RAX, 0
            je getCell

            ; R12 wird index
            mov R12, RAX
            DEC R12

            XOR RAX, RAX
            mov AL, byte [cells+R12]

            ; make sure cell is empty
            cmp AL, 0x00
            jne getCell

            mov byte [cells+R12], R8B
        
        call printBoard
        XOR R8B, 0xFF

        INC R9 ; increment Turn counter

        call checkGameOver

        cmp R9, 9
        jl gameLoop
    
    mov RDI, message_gameover
    mov RSI, message_gameover_l
    call printl
    mov RDI, message_no_winner
    mov RSI, message_no_winner_l
    call printl

    exit:
    ; exit(0)
    mov RDI, 0
    mov RAX, 60
    syscall



checkGameOver:
    ; 001 check top row
    mov AL, byte [cells+0]
    cmp AL, byte [cells+1]
    jne e_001
    cmp AL, [cells+2]
    jne e_001
    call printWinner
    e_001:

    ; 002 check left column
    cmp AL, byte [cells+3]
    jne e_002
    cmp AL, byte [cells+6]
    jne e_002
    call printWinner
    e_002:

    ; 003 check diagonal from top left to bottom right
    cmp AL, byte [cells+4]
    jne e_003
    cmp AL, byte [cells+8]
    jne e_003
    call printWinner
    e_003:

    ; 004 center row
    mov AL, byte [cells+4]
    cmp AL, byte [cells+3]
    jne e_004
    cmp AL, byte [cells+5]
    jne e_004
    call printWinner
    e_004:

    ; 005 center column
    cmp AL, byte [cells+1]
    jne e_005
    cmp AL, byte [cells+7]
    jne e_005
    call printWinner
    e_005:

    ; 006 diagonal from bottom left to top right
    cmp AL, byte [cells+2]
    jne e_006
    cmp AL, byte [cells+6]
    jne e_006
    call printWinner
    e_006:

    ; 007 right column
    mov AL, byte [cells+8]
    cmp AL, byte [cells+5]
    jne e_007
    cmp AL, byte [cells+2]
    jne e_007
    call printWinner
    e_007:

    ; 008 bottom row
    cmp AL, byte [cells+7]
    jne e_008
    cmp AL, byte [cells+6]
    jne e_008
    call printWinner
    e_008:
    ret

; prints who won
; if AL is 0 doesn't print
; AL: winner ID
printWinner:
    cmp AL, 0x0F
    jg pW_start_02
    je pW_start_01
    ret
    pW_start_01:
    call printGameOver
    mov RDI, message_winner_one
    mov RSI, message_winner_one_l
    jmp pW_print
    pW_start_02:
    call printGameOver
    mov RDI, message_winner_two
    mov RSI, message_winner_two_l
    pW_print:
    call printl
    jmp exit

printGameOver:
    mov RDI, message_gameover
    mov RSI, message_gameover_l
    call printl
    ret

printBoard:
    XOR RAX, RAX
    ;;; Row 0-2
    ;; Cell 0
    mov RAX, 0
    call printCell
    call printBoardSep
    ;; Cell 1
    mov RAX, 1
    call printCell
    call printBoardSep
    ;; Cell 2
    mov RAX, 2
    call printCell
    call printNL

    ;;; Row 3-5
    ;; Cell 3
    mov RAX, 3
    call printCell
    call printBoardSep
    ;; Cell 4
    mov RAX, 4
    call printCell
    call printBoardSep
    ;; Cell 5
    mov RAX, 5
    call printCell
    call printNL

    ;;; Row 6-8
    ;; Cell 6
    mov RAX, 6
    call printCell
    call printBoardSep
    ;; Cell 7
    mov RAX, 7
    call printCell
    call printBoardSep
    ;; Cell 8
    mov RAX, 8
    call printCell
    call printNL
    ret

printBoardSep:
    mov RDI, BOARD_SEP
    mov RSI, 1
    call print
    ret

; prints ' ', 'X', 'O' according to cells
; RAX: Cell Index
printCell:
    XOR RDX, RDX
    mov DL, byte [cells+RAX]
    cmp RDX, 0x0F
    je printCell_Player1
    jg printCell_Player2

    ; Cell Empty
    mov RDI, CELL_EMPTY
    mov RSI, 1
    call print
    ret

    ; Cell of Player 1
    printCell_Player1:
    mov RDI, CELL_P1
    mov RSI, 1
    call print
    ret

    ; Cell of Player 2
    printCell_Player2:
    mov RDI, CELL_P2
    mov RSI, 1
    call print
    ret

; print Intro Text
printIntro:
    mov RDI, message_intro1
    mov RSI, message_intro1_l
    call printl
    mov RDI, message_intro2
    mov RSI, message_intro2_l
    call printl
    mov RDI, message_intro3
    mov RSI, message_intro3_l
    call printl
    mov RDI, message_intro4
    mov RSI, message_intro4_l
    call printl
    mov RDI, message_intro5
    mov RSI, message_intro5_l
    call printl
    ret

printP1:
    mov RDI, message_player_one
    mov RSI, message_player_one_l
    call printl
    ret

printP2:
    mov RDI, message_player_two
    mov RSI, message_player_two_l
    call printl
    ret
    

; Gets a decimal digit and prints a string before every attempt
; returns via RAX
; RDI: pointer to a string
; RSI: length of the string
getDigit:
    mov R10, RDI
    mov R12, RSI

    startAttempt:
        mov RDI, R10
        mov RSI, R12
        call print

        ; read from stdin
        mov RAX, SYS_READ
        mov RDI, STD_IN
        mov RSI, buf
        mov RDX, 8
        syscall

        ; start over if only one character (\n)
        cmp RAX, 1
        jle startAttempt

        ; start over if more than two characters
        cmp RAX, 2
        jg startAttempt

        ; start over if below ascii digit range
        cmp byte [buf], 47
        jle startAttempt

        ; start over if over ascii digit range
        cmp byte [buf], 58
        jge startAttempt

    XOR RAX, RAX
    mov AL, byte [buf]
    SUB RAX, 48
    ret

; same as print, but prints a new line after
printl:
    ; call print
    call print
    call printNL
    ret

; prints a new line
; takes no arguments
printNL:
    mov RDI, NEW_LINE
    mov RSI, 1
    call print
    ret

; print without newline
; RDI: pointer to a string   (8 Byte)
; RSI: length of the string  (8 Byte)
print:
    mov RDX, RSI
    mov RSI, RDI
    mov RAX, SYS_WRITE
    mov RDI, STD_OUT
    syscall
    ret

section .data
SYS_READ:               equ 0
SYS_WRITE:              equ 1

STD_IN:                 equ 0
STD_OUT:                equ 1

NEW_LINE:               db 0xA
BOARD_SEP:              db "|"
CELL_EMPTY:             db " "
CELL_P1:                db "X"
CELL_P2:                db "O"

message_intro1:         db "This Tic-Tac-Toe Game is brought to you by x86_64 Assembly!"
message_intro1_l:       equ $-message_intro1
message_intro2:         db "These are the indices of the cells:"
message_intro2_l:       equ $-message_intro2
message_intro3:         db "1|2|3"
message_intro3_l:       equ $-message_intro3
message_intro4:         db "4|5|6"
message_intro4_l:       equ $-message_intro4
message_intro5:         db "7|8|9"
message_intro5_l:       equ $-message_intro5

message_player_one:     db "Player 1's Turn (X): "
message_player_one_l:   equ $-message_player_one
message_player_two:     db "Player 2's Turn (O): "
message_player_two_l:   equ $-message_player_two
message_enter_cell:     db "Enter Cell Number (1-9): "
message_enter_cell_l:   equ $-message_enter_cell

message_gameover:       db "Game Over!"
message_gameover_l:     equ $-message_gameover
message_no_winner:      db "Nobody won."
message_no_winner_l:    equ $-message_no_winner
message_winner_one:     db "The Winner is: Player 1 !!!"
message_winner_one_l:   equ $-message_winner_one
message_winner_two:     db "The Winner is: Player 2 !!!"
message_winner_two_l:   equ $-message_winner_two

section .bss
buf: resb 8
cells: resb 9